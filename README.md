# Matlab Visual Servoing 

Visual servoing library to drive a holonomic camera (6DoF).

Currently implemented visual servo (VS) schemes: 

  * Classical image-based VS (IBVS)

  * Uncalibrated image-based VS (UIBVS)

  * Classical position-based VS (PBVS) 

**IMPORTANT NOTE**: This library is not optimized, please use the C++ implementation for scientific comparison purposes. 

&nbsp;

## If you use this code, please cite:

[Uncalibrated Visual Servo for Unmanned Aerial Manipulation](http://www.angelsantamaria.eu/publications/tmech17)

Angel Santamaria-Navarro, Patrick Grosch, Vincenzo Lippiello, Joan Solà and Juan Andrade-Cetto

IEEE/ASME Transactions on Mechatronics, 2017, vol. 22, no. 4, pp. 1610-1621.

&nbsp;

[Uncalibrated image-based visual servoing](http://www.angelsantamaria.eu/publications/icra13)

A. Santamaria-Navarro and J. Andrade-Cetto

IEEE International Conference on Robotics and Automation, 2013, Karlsruhe, pp. 5227-5232.

&nbsp;

[Hybrid Visual Servoing with Hierarchical Task Composition for Aerial Manipulation](http://www.angelsantamaria.eu/publications/ral16)

V. Lippiello, J. Cacace, A. Santamaria-Navarro, J. Andrade-Cetto, M.A. Trujillo, Y. Rodriguez Esteves and A. Viguria

IEEE Robotics and Automation Letters, 2016, vol. 1, no. 1, pp. 259-266

&nbsp;

## Example of usage:

- Run `main.m` script.
- Initial and final desired camera poses are defined in `.mat` files stored in `data/` folder.
- Initial parameters are set in `functions/Ini_params` script.
- Plot options are set inside `main.m`  

## Support material and multimedia

Please, visit: [**asantamaria's web page**](http://www.angelsantamaria.eu)