% Copyright (C) 
% Author asantamaria (asantamaria@iri.upc.edu)
% All rights reserved.
% 
% This file is part of VisualServo Matlab library
% VisualServo library is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>
%
%   __________________________________________________________________
%
%   function [T0c_x,T0c_0,T0o,lambda,Ts,t,diTj,pn,u0,v0,f_uibvs,f_real,f_ibvs,hres,vres,noise] = ini_params()
% 
%   This function initializes the following parameters:
% 
%       - T0c_x:    Desired final camera pose (4x4 homogenous transform).
%       - T0c_0:    Initial camera pose (4x4 homogenous transform).
%       - T0o:      Object pose in world frame (4x4 homogenous transform).
%       - lambda:   Proportional control gain.
%       - Ts:       Time step.
%       - t:        Time sequence.
%       - diTj:     alphas.
%       - pn:       3D feature points.
%       - u0:       Center of the Image (horizontal).
%       - v0:       Center of the Image (vertical).
%       - f_real:   Real focal length.
%       - f_ibvs:   Focal length used to initialize IBVS.
%       - f_uibvs:  Focal length used to initialize UIBVS. 
%       - f_pbvs:   Focal length used to initialize PBVS. 
%       - hres:     Image horizontal resolution.
%       - vres:     Image vertical resolution.
%       - noise:    Noise in image detection (pixels).
% 
%   Copyright asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [T0c_x,T0c_0,T0o,lambda,Ts,t,diTj,pn,u0,v0,f_real,f_ibvs,f_uibvs,f_pbvs,hres,vres,noise] = ini_params()

% Camera desired position
load pose_fi
T0c_x = T0c; 	

% Camera initial position
load pose_ini
T0c_0 = T0c;	
T0o=eye(4,4);     

% Control Law gain
lambda = 0.125;   

% Time step
Ts=0.1;

% Time sequence
t=0:Ts:50;   
% Initial alphas
diTj=zeros(4,4); 

% Number of points
n=9;

% Object

% Random 3D object features
% pn=0.5*randn(3,n);
% pn=coords(polyhedra('polygon', n, 1));
% pn(4,:)=ones(1,n);
% pn(3,:)=pn(2,:)-pn(1,:);
% pn(3,:)=ones(1,n);

% Non random 3D object features
pn=[   -0.1247    0.3675    0.2719   -0.1693   -0.3662    0.4121    1.0902    0.1989   -0.0788
   -0.7091   -0.4199    0.2718    0.2099    0.2251   -0.6189    0.5079    0.7565   -0.0277
   -0.2759   -0.0641    0.3392    0.0662   -0.1067    0.1483    0.3323    1.0093    0.5745
    1.0000    1.0000    1.0000    1.0000    1.0000    1.0000    1.0000    1.0000    1.0000];   
    
% real focal dist. 
f_real=100; 
% If f_ibvs and f_uibvs differ from f_real, it is the wrong focal length 
% initialization, or unaccounted camera zoom: e.g. 20% error
% Focal length if a zoom is performed, used to initialize IBVS. 
% Depending on lambda, it will require larger or smaller focal length changes. 
% With lambda=0.125, the unstabilization can be seen with 
% f_ibvs=f_real*1.331;
f_ibvs=f_real;
% simulated focal dist. used to initialize UIBVS
f_uibvs=f_ibvs;
% simulated focal length used to initialize PBVS
f_pbvs=f_ibvs;

% Image resolution
hres=640;
vres=480;
u0=hres/2;
v0=vres/2;

% Standard deviation of image noise
noise=0;