% Copyright (C) 
% Author asantamaria (asantamaria@iri.upc.edu)
% All rights reserved.
% 
% This file is part of VisualServo Matlab library
% VisualServo library is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>
%
%   __________________________________________________________________
%
%   [J]=computeJacobian(alphas,u0,v0,uvn,cj_0,n)
% 
%   This function computes the uncalibrated Jacobian. 
%   Refer to the following paper for more details:
% 
%   A. Santamaria-Navarro and J. Andrade-Cetto, 
%   Uncalibrated image-based visual servoing, 
%   IEEE International Conference on Robotics and Automation,
%   2013, Karlsruhe, pp. 5227-5232.
%    
%   Inputs:
%       - alphas:   Alphas (aij's)  
%       - u0:       Center of the Image (horizontal)
%       - v0:       Center of the Image (vertical)  
%       - uvn:      Object coordinates in cam frame using f. simulated
%       - cj_0:     Control points 
%       - n:        Number of original 3D feature points
% 
%   Outputs:
%       - J: Uncalibrated Jacobian
% 
%   Copyright asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [J,f_simu]=computeJacobian(alphas,u0,v0,uvn,cj_0,n)

%% Non planar case
if (length(cj_0)>3)
    
    % Generate M and check the rank
    for i=1:n
        for j=1:4
            % u rows
            M(2*i-1,j*3-2)=alphas(j,i);
            M(2*i-1,j*3)=alphas(j,i)*(u0-uvn(1,i));
            % v rows
            M(2*i,j*3-1)=alphas(j,i);
            M(2*i,j*3)=alphas(j,i)*(v0-uvn(2,i));
        end
    end
    
    [V,~]=eig(M'*M);
   
    % N=1 case
    v=V(:,1);

    % Distance equations between control points
    g=0;
    for i=1:3
        for j=i+1:4
           g=g+1;
           va=v(i*3-2:i*3,1);
           vb=v(j*3-2:j*3,1);
           vc=va-vb;
           L(g,1)=vc(1:2)'*vc(1:2);
           L(g,2)=vc(3)*vc(3);
           d(g,1)=(cj_0(:,i)-cj_0(:,j))'*(cj_0(:,i)-cj_0(:,j));
        end
    end
     
    rank(L);


    % Obtaining Beta and the focal distance
    b=L\d;
    
    % Choosing an initial sign of Beta
    B = +sqrt(b(1)); 

    f_simu = +sqrt(b(2))/B;

    % obtaining C's
    c2 = real(B*v);

    
    % Correct sign selection of B looking at the distance relationships of the base points
    
     cc2=c2(4:6)-c2(1:3); cc3=c2(7:9)-c2(1:3); cc4 = c2(10:12)-c2(1:3);

    oo1=cross(cc2,cc3)'*cc4;
    oo2=cross(cc2,cc4)'*cc3;
    oo3=cross(cc3,cc4)'*cc2;
    
    if(oo1 < 0 || oo2 > 0 || oo3 < 0)
       c2 = -c2;
    end;

    c2([3 6 9 12],1) = c2([3 6 9 12],1)*f_simu;


    % Visual Jacobian
    fi=0;
    J=zeros(8,6);
    for i=1:4
    xj=c2([i*3-2],1);
    yj=c2([i*3-1],1);
    zj=c2([i*3],1);
    J([fi+1],:)=[(-1/zj) 0 (xj/zj) (xj*yj) -(1+xj^2) (yj)];
    J([fi+2],:)=[0 (-1/zj) (yj/zj) (1+yj^2) -(xj*yj) -(xj)];
    fi=fi+2;
    end

    J = real(J); % choose only real elements
    J(isnan(J))=0;
    J(isinf(J))=0;

%% Planar case    
else
    
    
    % Generate M and check the rank
    for i=1:n
        for j=1:3
            % u rows
            M(2*i-1,j*3-2)=alphas(j,i);
            M(2*i-1,j*3)=alphas(j,i)*(u0-uvn(1,i));
            % v rows
            M(2*i,j*3-1)=alphas(j,i);
            M(2*i,j*3)=alphas(j,i)*(v0-uvn(2,i));
        end
    end
    
     
    [V,S]=eig(M'*M);

    % N=1 case
    v=V(:,1);
    
    % Distance equations between control points
    g=0;
    for i=1:2
        for j=i+1:3
           g=g+1;
           va=v(i*3-2:i*3,1);
           vb=v(j*3-2:j*3,1);
           vc=va-vb;
           L(g,1)=vc(1:2)'*vc(1:2);
           L(g,2)=vc(3)*vc(3);
           d(g,1)=(cj_0(:,i)-cj_0(:,j))'*(cj_0(:,i)-cj_0(:,j));
        end
    end
    
    rank(L);

    % Obtaining Beta and the focal distance
    b=L\d;
    % Choosing an initial sign of Beta
    B = +sqrt(b(1));

    f_simu = +sqrt(b(2))/B;
    
    % obtaining C's
    c2 = B*v;
    
    % Correct sign selection of B looking at the distance relationships of the base points
    
    oo=det([c2(1:3) c2(4:6) c2(7:9)]);
    if(oo > 0)
        c2 = -c2;
    end;

    c2([3 6 9],1) = c2([3 6 9],1)*f_simu;


    % Visual Jacobian
    fi=0;
    J=zeros(6,6);
    for i=1:3
    xj=c2([i*3-2],1);
    yj=c2([i*3-1],1);
    zj=c2([i*3],1);
    J([fi+1],:)=[(-1/zj) 0 (xj/zj) (xj*yj) -(1+xj^2) (yj)];
    J([fi+2],:)=[0 (-1/zj) (yj/zj) (1+yj^2) -(xj*yj) -(xj)];
    fi=fi+2;
    end  
    
    J = real(J); % choose only real elements
    J(isnan(J))=0;
    J(isinf(J))=0;
end

end