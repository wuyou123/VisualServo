% Copyright (C) 
% Author asantamaria (asantamaria@iri.upc.edu)
% All rights reserved.
% 
% This file is part of VisualServo Matlab library
% VisualServo library is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>
%
%   __________________________________________________________________
%
%   [sk] = skew(vec)
% 
%   This function computes the skew-symmetric matrix from a vector.
%    
%   Inputs:
%       - vec:  Vector (3x1 or 1x3).           
% 
%   Outputs:
%       - sk:   Skew symmetric matrix (3x3).                             
% 
%   Copyright asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [sk] = skew(vec)

sk=[0 -vec(3) vec(2) ; vec(3) 0 -vec(1) ; -vec(2) vec(1) 0 ];

end