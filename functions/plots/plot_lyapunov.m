% Copyright (C) 
% Author asantamaria (asantamaria@iri.upc.edu)
% All rights reserved.
% 
% This file is part of VisualServo Matlab library
% VisualServo library is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>
%
%   __________________________________________________________________
%
%   Function to plot Lyapunov stability proof for UIBVS
% 
%   Inputs:
%       - nfig:         Number of figure.
%       - t:            Time sequence.
%       - data:         Lyapunov function and function derivative values.   
%       - win_name:     Window name.
% 
%   Outputs:
%       - hfig:         Figure handler.
% 
%   Copyright asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [hfig] = plot_lyapunov(nfig,t,data,win_name)

% Figure properties
fig.num = nfig;
fig.title = win_name;
fig.fontsize = 35;
fig.position =  [100, 100, 1000, 400];
fig.linewidth = 3;
fig.labels.x = 's';
fig.labels.y = '$\mathcal{L};\dot{\mathcal{L}}$';

% names
x.name = '$t$';
y{1}.name = '$\mathcal{L} = \frac{1}{2}||{\bf e}(t)||^2$';
y{2}.name = '$\dot{\mathcal{L}} = -\lambda{\bf e}^\top{\bf J}{\bf J}^+{\bf e}$';

% line styles
ls{1} = '-';
ls{2} = '--';

% colors
color = [[0 0 1];[0 0 0]];

% fill structures
x.data = t;
for ii=1:size(data,2)
    y{ii}.data = data(:,ii);
    y{ii}.color = color(ii,:);
    y{ii}.linestyle = ls{ii};
end

hfig = generic_plot_data(fig,x,y);

end