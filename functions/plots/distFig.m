% 
% Function to distribute figures in the screen. 
% Extracted from: https://es.mathworks.com/matlabcentral/fileexchange/37176-distribute-figures/content/distFig.m
% 
% Syntax:
% 
% distFig(...,'Screen',Value)	/ distFig(...,'Scr',Value)
% distFig(...,'Position',Value) / distFig(...,'Pos',Value)
% distFig(...,'Rows',Value)
% distFig(...,'Not',Value)
% distFig(...,'Only',Value)
% distFig(...,'Offset',Value)
% 
%
% Description:
% 
% distFig(...,'Screen',Value) assigns where the figures will be
% distributed. Value can be:
% 'Left' / 'L'
% 'Right' / 'R'
% 'Top' / 'T'
% 'Center' / 'C' (Default)
%
% distFig(...,'Position',Value) assigns in which part of the screen the
% figures will be distributed. Value can be:
% 'Left' / 'L'
% 'Right' / 'R'
% 'Center' / 'C' (Default)
%
% distFig(...,'Rows',Value) assigns how many rows the figures will be
% distributed on. Value must be an integer larger than zero. The default
% number of rows is 2. 
% 
% distFig(...,'Not',Value) excludes specified figures from the distribution
% list. Value must be an matrix with the excluded figure numbers.
%
% distFig(...,'Only',Value) does only distrubute specified figures. Value
% must be an matrix with the figure which will be destributed.
% 
% distFig(...,'Offset',Value) can be used to shift all figures on the
% distribution list. Value must be an integer larger or equal to zero. The
% default offset value is 0.
%
%
% Examples:
%
% distFig();
% This will distribute all open figures on the primary screen in two rows.
%
% distFig('Screen','Left','Position','Right','Only',[1,2,4])
% This will only distrubute figure 1, 2 and 4 on the right part of the left
% screen.
%
% distFig('Offset',2,'Not',[1,2])
% This will distribute all figure but figure 1 and 2 in the same pattern as
% distFig(), but figure 1 and 2 will not be distributed, but instead there will
% be blank spots where they would have been distributed.

function distFig(varargin)

% ==================================
% ===== Get figures ================
% ==================================

Figure_List = findall(0,'type','figure');
if (isempty(Figure_List) == 1)
	fprintf('No figures to distribute.\n');
	return;
end

% ==================================
% ===== Default values =============
% ==================================

Position = 'Center';
Screen = 'Center';
Rows = 2; 
Offset = 0; 

% ==================================
% ===== Get input ==================
% ==================================

if (nargin > 0)
	for i = 1:2:nargin
		if (ischar(varargin{i}))
			switch lower(varargin{i})
				case {'position','pos'}
					switch lower(varargin{i+1})
						case {'right','r'}
							Position = 'Right';
						case {'left','l'}
							Position = 'Left';
						case {'center','c'}
							% Default
						otherwise
							if (ischar(varargin{i+1}))
								warning('''%s'' is not a valid input for property ''Position''.',varargin{i+1});
							else
								warning('Invalid input type for property ''Position''.');
							end
					end
				case {'screen','scr'}
					switch lower(varargin{i+1})
						case {'right','r'}
							Screen = 'Right';
						case {'left','l'}
							Screen = 'Left';
						case {'top','t'}
							Screen = 'Top';
						case {'center','c'}
							% Default
						otherwise
							if (ischar(varargin{i+1}))
								warning('''%s'' is not a valid input for property ''Screen''.',varargin{i+1});
							else
								warning('Invalid input type for property ''Screen''.');
							end
					end
				case 'rows'
					if (isnumeric(varargin{i+1}))
						Rows = varargin{i+1};
					else
						warning('Invalid input type for property ''Rows''.');
					end
				case 'not'
					if (isnumeric(varargin{i+1}))
						Temp = varargin{i+1};
						for j = 1:length(Temp)
							Figure_List = Figure_List(Figure_List ~= Temp(j));
						end
					else
						warning('Invalid input type for property ''Not''.');
					end
				case 'only'
					if (isnumeric(varargin{i+1}))
						Temp1 = Figure_List;
						Temp2 = varargin{i+1};
						Figure_List = [];
						for j = 1:length(Temp2)
							Figure_List = [Figure_List , Temp1(Temp1 == Temp2(j))];
						end
					else
						warning('Invalid input type for property ''Only''.');
					end
				case 'offset'
					if (isnumeric(varargin{i+1}))
						Offset = varargin{i+1};
					else
						warning('Invalid input type for property ''Offset''.');
					end
				otherwise
					warning('''%s'' is not a valid input for distFig.',varargin{i});
			end
		else
			warning('Invalid input type for distFig.');
		end
	end
end

Figure_List = sort(Figure_List);

% ==================================
% ===== Calculate sizes ============
% ==================================

Monitor = get(0,'ScreenSize');
Figure_n = length(Figure_List) + Offset;
Figure_n_H = ceil(Figure_n / Rows);
Figure_Height = (Monitor(4) - 40) / Rows;

if (strcmp(Position,'Center'))
	Figure_Width = Monitor(3) / Figure_n_H;
	Figure_x = linspace(0,Monitor(3) - Figure_Width,Figure_n_H);
else
	Figure_Width = (Monitor(3) / 2) / Figure_n_H;
	Figure_x = linspace(0,Monitor(3) / 2 - Figure_Width,Figure_n_H);
	if (strcmp(Position,'Right'))
		Figure_x = Figure_x + Monitor(3) / 2;
	end
end

if (strcmp(Screen,'Left'))
	Figure_x = Figure_x - Monitor(3);
elseif (strcmp(Screen,'Right'))
	Figure_x = Figure_x + Monitor(3);
end

Figure_y = linspace(40,Monitor(4) - Figure_Height,Rows);

Figure_X = [];
Figure_Y = [];

for i = 1:Rows
	Figure_X = [Figure_X , Figure_x];
	Figure_Y = [ones(1,length(Figure_x)) * Figure_y(i) , Figure_Y];
end

if (strcmp(Screen,'Top'))
	Figure_Y = Figure_Y + Monitor(4);
end

% ==================================
% ===== Move figures ===============
% ==================================

for i = 1:length(Figure_List)
	set(figure(Figure_List(i)),'OuterPosition',[Figure_X(i + Offset) , Figure_Y(i + Offset) , Figure_Width , Figure_Height])
end