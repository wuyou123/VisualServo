% Copyright (C) 
% Author asantamaria (asantamaria@iri.upc.edu)
% All rights reserved.
% 
% This file is part of VisualServo Matlab library
% VisualServo library is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>
%
%   __________________________________________________________________
%
%   Function to plot camera velocities (6 DoF) during a trajectory.
% 
%   Inputs:
%       - nfig:     Figure number.
%       - t:        Time sequence. 
%       - data:     Camera velocities (6 DoF) over t period.
%       - win_name: Window name.
% 
%   Outputs:
%       - hfig:     Figure handler.
% 
%   Copyright asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [hfig] = plot_velocity(nfig,t,data,win_name)

% Figure properties
fig.num = nfig;
fig.title = win_name;
fig.fontsize = 35;
fig.position =  [100, 100, 1000, 500];
fig.linewidth = 3;
fig.labels.x = 's';
fig.labels.y = 'm;rad';

% names
x.name = '$t$';
y{1}.name = '$\nu_{x}$';
y{2}.name = '$\nu_{y}$';
y{3}.name = '$\nu_{z}$';
y{4}.name = '$\omega_{x}$';
y{5}.name = '$\omega_{y}$';
y{6}.name = '$\omega_{z}$';

% line styles
ls{1} = '-';
ls{2} = '--';
ls{3} = '-.';
ls{4} = '-';
ls{5} = '--';
ls{6} = '-.';

% colors
color = [repmat([0 0 1],3,1);repmat([0 0 0],3,1)];

% fill structures
x.data = t;
for ii=1:size(data,1)
    y{ii}.data = data(ii,:);
    y{ii}.color = color(ii,:);
    y{ii}.linestyle = ls{ii};
end

hfig = generic_plot_data(fig,x,y);

end

