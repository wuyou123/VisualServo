% Copyright (C) 
% Author asantamaria (asantamaria@iri.upc.edu)
% All rights reserved.
% 
% This file is part of VisualServo Matlab library
% VisualServo library is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>
%
%   __________________________________________________________________
%
%   Function to plot trajectories of features in the image plane
% 
%   Inputs:
%       - nfig:         Number of figure.
%       - uv_des:       Desired features in image plane.
%       - uv_ibvs:      IBVS current features in image plane.
%       - uv_uibvs:     UIBVS current features in image plane.
%       - uv_pbvs:      PBVS current features in image plane.
%       - win_name:     Window name.
% 
%   Outputs:
%       - hfig:         Figure handler.
% 
%   Copyright asantamaria@iri.upc.edu.
%   __________________________________________________________________

% Plotting Image feature points trajectories

function [hfig] = plot_pointstraj(nfig,uv_ini,uv_des,uv_ibvs,uv_uibvs,uv_pbvs,win_name)

ui = uv_ini(1,:,1);
vi = uv_ini(2,:,1);
ue = uv_des(1,:,1);
ve = uv_des(2,:,1);

ibvs.u(:,:) = uv_ibvs(1,:,:);
ibvs.v(:,:) = uv_ibvs(2,:,:);
uibvs.u(:,:) = uv_uibvs(1,:,:);
uibvs.v(:,:) = uv_uibvs(2,:,:);
pbvs.u(:,:) = uv_pbvs(1,:,:);
pbvs.v(:,:) = uv_pbvs(2,:,:);

% Figure properties
fig.num = nfig;
fig.title = win_name;
fig.fontsize = 25;
fig.position =  [300, 300, 700, 700];
fig.linewidth = 3;
fig.labels.x = 'u (pixels)';
fig.labels.y = 'v (pixels)';

hfig = figure(fig.num);
hold on;

for i=1:size(pbvs.u,1)
    p1 = plot(pbvs.u(i,:),pbvs.v(i,:),'k:','LineWidth',fig.linewidth);
end

for i=1:size(ibvs.u,1)
    p2 = plot(ibvs.u(i,:),ibvs.v(i,:),'b-','LineWidth',fig.linewidth);
end

for i=1:size(uibvs.u,1)
    p3 = plot(uibvs.u(i,:),uibvs.v(i,:),'r-.','LineWidth',fig.linewidth);
end

p4 = plot(ui,vi,'ko','MarkerSize',10,'LineWidth',fig.linewidth);
p5 = plot(ue,ve,'gx','MarkerSize',10,'LineWidth',fig.linewidth);

xlabel(fig.labels.x);
ylabel(fig.labels.y);

xmin = min(min(min(min(min(ibvs.u))),min(min(min(uibvs.u)))),min(min(min(pbvs.u))));
xmax = max(max(max(max(max(ibvs.u))),max(max(max(uibvs.u)))),max(max(max(pbvs.u))));
ymin = min(min(min(min(min(ibvs.v))),min(min(min(uibvs.v)))),min(min(min(pbvs.v))));
ymax = max(max(max(max(max(ibvs.v))),max(max(max(uibvs.v)))),max(max(max(pbvs.v))));

axis([xmin-5 xmax+5 ymin-5 ymax+5]);

set(gca, 'Ydir', 'reverse')

hold off;
axis equal;
hleg = generic_figure_params(hfig,fig.title,fig.fontsize,fig.position);

% remove extra legend entries
legend('off'); legend('on');
hleg = legend([p1 p2 p3 p4 p5],'PBVS','IBVS','UIBVS','Start','Desired');
set(hleg,'Interpreter','latex','FontSize',fig.fontsize);

end
