% Copyright (C) 
% Author asantamaria (asantamaria@iri.upc.edu)
% All rights reserved.
% 
% This file is part of VisualServo Matlab library
% VisualServo library is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>
%
%   __________________________________________________________________
%
%   Function that creates the figure handlers needed to plot 
%   a 3D camera trajectory.
% 
%   Inputs:
%       - nfig:     Figure number.
%       - c:        Camera handler.
%       - T0c:      Array of camera poses (4x4 homogenous transformations).
%       - pn:       3D object feature points.
%       - cj_0:     3D control points.
%       - win_name: Window name.
% 
%   Copyright asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [hfig] = plot_camtraj(nfig,c,T0c,pn,cj_0,win_name)

hfig = figure(nfig);
hold on;

az=-20;
el=60;
view(az,el)
axis([-1 3 -4 2 -1 1.5]);

n = size(T0c,3);
plot(ht(T0c(:,:,1)),'start');

prevhold = ishold;

h1=plot3(cj_0(1,:),cj_0(2,:),cj_0(3,:),'rx','LineWidth',2,'MarkerSize',10);
h2=plot3(pn(1,:),pn(2,:),pn(3,:),'bo','LineWidth',2,'MarkerSize',10);  

if ~prevhold
    hold on;
end

for i=2:10:n-1
    plot(ht(T0c(:,:,i)));
end
plot(ht(T0c(:,:,end)),'end');

plot(c,ht(T0c(:,:,1)));
plot(c,ht(T0c(:,:,end)));

legend('Location','NorthEastOutside');
h4=legend([h1 h2],'Control points','Features (3D)');
set(h4,'fontsize',30,'position',[0.564 0.741 0.255 0.122],'Interpreter','latex');

if ~prevhold
    hold off;
end

axis([-1,2.5,-2.5,1,-0.4,1.2])
grid on
xlabel('x (m)','fontsize',30), ylabel('y (m)','fontsize',30);
zlabel('z (m)','fontsize',30);
set(gca,'FontSize',30);
set(gcf,'Color',[1,1,1]);
set(hfig,'NumberTitle','off','Name',win_name,'Renderer','Painters','defaulttextinterpreter','latex','Position',[100, 100, 800, 600]);
set(findall(gcf,'-property','TickLabelInterpreter'),'TickLabelInterpreter','latex')

end
