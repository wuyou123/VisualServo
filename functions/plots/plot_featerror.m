% Copyright (C) 
% Author asantamaria (asantamaria@iri.upc.edu)
% All rights reserved.
% 
% This file is part of VisualServo Matlab library
% VisualServo library is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>
%
%   __________________________________________________________________
%
%   This function creates the figure handlers to plot the errors 
%   between desired and current image plane features.
% 
%   Inputs:
%       - fig:          Figure handler.
%       - img_str:      Current image features.
%       - img_x_str:    Desired image features.    
%       - win_name:     Window name.
% 
%   Outputs:
%       - hfig:         Figure handler.
% 
%   Copyright asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [hfig] = plot_featerror(nfig,t,uv_curr,uv_des,win_name)

% Data
n = size(uv_curr,2);
s = reshape(uv_curr,n*2,length(t))';
s_x = repmat(reshape(uv_des,n*2,1)',length(t),1);

% Features error
for i=1:n*2
    data(i,:)=s(:,i)-s_x(:,i);
end

% Figure properties
fig.num = nfig;
fig.title = win_name;
fig.fontsize = 35;
fig.position =  [100, 100, 1000, 500];
fig.linewidth = 3;
fig.labels.x = 's';
fig.labels.y = 'pixels';

% names
x.name = '$t$';
y{1}.name = '$u_1$';
y{2}.name = '$u_2$';
y{3}.name = '$u_3$';
y{4}.name = '$u_4$';
y{5}.name = '$v_1$';
y{6}.name = '$v_2$';
y{7}.name = '$v_3$';
y{8}.name = '$v_4$';

% line styles
ls{1} = '-';
ls{2} = '--';
ls{3} = '-.';
ls{4} = ':';
ls{5} = '-';
ls{6} = '--';
ls{7} = '-.';
ls{8} = ':';

% Planar case (3 features)
if n==3
   y(8) = [];
   y(4) = [];
   ls(8) = [];
   ls(4) = [];
   color = [color(1:3,:);color(5:7,:)];
end

% colors
color = [repmat([0 0 1],n,1);repmat([0 0 0],n,1)];

% fill structures
x.data = t;
for ii=1:size(data,1)
    y{ii}.data = data(ii,:);
    y{ii}.color = color(ii,:);
    y{ii}.linestyle = ls{ii};
end

hfig = generic_plot_data(fig,x,y);

end
