% Copyright (C) by A. Santamaria-Navarro (asantamaria@iri.upc.edu)
%
% This file is part of MatlabPlot_constr_task_opt. You can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% QuadOdom is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Leser General Public License
% along with MatlabPlot_constr_task_opt.  If not, see <http://www.gnu.org/licenses/>.

function [hfig] = generic_plot_data(fig,x,y)

hfig = figure(fig.num);
hold on;

ymin = 0.0;
ymax = 0.0;
for ii = 1:size(y,2)
    plot(x.data,y{ii}.data,'Color',y{ii}.color,'LineWidth',fig.linewidth,'LineStyle',y{ii}.linestyle,'DisplayName',y{ii}.name);
    if (min(y{ii}.data) < ymin)
        ymin = min(y{ii}.data);
    end
    if (max(y{ii}.data) > ymax)
        ymax = max(y{ii}.data);   
    end
end

xl = xlabel(fig.labels.x);
set(xl,'interpreter','LaTex')

yl = ylabel(fig.labels.y);
set(yl,'interpreter','LaTex')

% Center at zero
if (abs(ymin)>abs(ymax))
    ymax = -ymin;
else
    ymin = -ymax;
end

axis([x.data(1) x.data(end) ymin ymax]);

hold off;

generic_figure_params(hfig,fig.title,fig.fontsize,fig.position);

end