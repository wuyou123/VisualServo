% Copyright (C) 
% Author asantamaria (asantamaria@iri.upc.edu)
% All rights reserved.
% 
% This file is part of VisualServo Matlab library
% VisualServo library is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>
%
%   __________________________________________________________________
%
%   [Vc,T0c_new,uv_pbvs] = pbvs(T0o,T0c_x,T0c_0,cj_0,f_pbvs,f_real,u0,v0,lambda,t,Ts,diTj,noise,pn)
% 
%   This function computes a calibrated pose based visual servo
% 
%   Inputs:
%       - T0o:      Object pose in world frame (4x4 homogenous transform)            
%       - T0c_x:    Desired final camera pose (4x4 homogenous transform)            
%       - T0c_0:    Initial camera pose (4x4 homogenous transform)
%       - cj_0:     Control points   
%       - f_pbvs:   Focal length in case of a zoom  
%       - f_real:   Real focal length 
%       - u0:       Center of the Image (horizontal)
%       - v0:       Center of the Image (vertical)        
%       - lambda:   Proportional control gain           
%       - t:        Time sequence               
%       - Ts:       Time step            
%       - diTj:     Integral condition                     
%       - noise:    Standard deviation of image noise    
%       - pn:       Original 3D features
% 
%   Outputs:
%       - Vc:           Camera velocities           
%       - T0c_new:      Camera pose during trajectory
%       - uv_pbvs:      Features in image plane (plot purposes)
% 
%   Copyright asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [Vc,T0c_new,uv_pbvs] = pbvs(T0o,T0c_x,T0c_0,cj_0,f_pbvs,f_real,u0,v0,lambda,t,Ts,diTj,noise,pn)

disp('Computing PBVS');

diTj_int(:,:,1)=diTj;
   
%% simulating all time intervals _________________________________
for k=1:length(t)
    
%   % White noise appearance    
%   r = 10*(-1 + (1+1).*rand(1));
%   f_real=f_real+r;
    
    % Camera calibration matrix
    Kmat = [f_pbvs 0 u0; 0 f_pbvs v0;0 0 1];
    
    % Camera update r.t. world frame
    T0c_new(:,:,k)=diTj_int(:,:,k)+T0c_0;   

    % Features pixel coordinates to plot
    [uv_pbvs(:,:,k)]=imageprojection(f_real,f_real,u0,v0,inv(T0c_new(:,:,k))*T0o*[cj_0;ones(1,length(cj_0))]);
    [uv_pbvs(:,:,k)]=[uv_pbvs(:,:,k)]+noise*randn(2,length(uv_pbvs(:,:,k)));
     
    % Get HT from current to desired poses __________________
    % Features transform r.t. camera frame
    cP=inv(T0c_new(:,:,k))*T0o*pn;
    % Features pixel coordinates to run EPnP
    [uv(:,:,k)]=imageprojection(f_real,f_real,u0,v0,inv(T0c_new(:,:,k))*T0o*cP);
    [uv(:,:,k)]=[uv(:,:,k)]+noise*randn(2,length(uv(:,:,k)));
    % EPnP
    [Rcurr, tcurr,~,~] = efficient_pnp(cP', uv(:,:,k)', Kmat);
    cT0 = [Rcurr tcurr;0 0 0 1];
    % W/o EPnP  
    % cT0 = inv(T0c_new(:,:,k))*T0o;

    % Error    
    cxT0 = inv(T0c_x)*T0o;
    cxTc = cxT0*inv(cT0);
    err = getvaldof(cxTc);  
    
    % %Angular error using quaternions 
    %q1 = quatnormalize(dcm2quat(cT0(1:3,1:3))); 
    %q2 = quatnormalize(dcm2quat(cxT0(1:3,1:3)));  
    %sca = q1(1)*q2(1)+dot(q1(2:4),q2(2:4)); % Signs changes because q1 is conjugate
    %vec = -q2(1)*q1(2:4)'+q1(1)*q2(2:4)'-skew(q1(2:4))*q2(2:4)'; % Signs changes because q1 is conjugate
    %err2 = getvaldof([quat2dcm([sca;vec]') [0;0;0];0 0 0 1]);

    % Angular Jacobian
    % Angle-axis formulation   
    theta = norm(err(4:6));
    u = err(4:6)/theta;
    Lthu = eye(3)-0.5*theta*skew(u)+(1-(sinc(theta)/(sinc(0.5*theta)^2)))*skew(u).^2;   
    % % Rotation matrix formulation
    %cxRc = cxTc(1:3,1:3);
    %Lthu2 = 0.5*(trace(cxRc')*eye(3) - cxRc);
    % % Quaternion formulation
    %qq = quatnormalize(dcm2quat(cxRc'));
    %Lthu3 = 0.5*qq(1)*eye(3)-skew(qq(2:4));    
    
    J = [cxTc(1:3,1:3) zeros(3);zeros(3) Lthu];
    
    % Jacobian pseudo-inverse
    J_pseudo=pinv(J);
    % Proportional control
    Vc(:,k)=-lambda*J_pseudo*err;
    vel=Vc(:,k);
    % Lineal Velocity
    v_linear(1:3,1)=Vc(1:3,k);
    % Angular Velocity
    v_angular(1:3,1)=Vc(4:6,k);
    a=v_angular;
    % Skew-symmetric matrix 
    skew_v_angular= [0 -a(3) a(2) ; a(3) 0 -a(1); -a(2) a(1) 0];

    % Using modified velocity matrix 
    % X_dot=v_angular*X+v_linear
    V_modified=[skew_v_angular, v_linear;0 0 0 0];

    Vc(:,k)=vel;
    % Diferential change of T0c under Vc
    diTj(:,:,k)=T0c_new(:,:,k)*V_modified;
    % Integration aproximation
    diTj_int(:,:,k+1)=diTj_int(:,:,k)+diTj(:,:,k)*Ts;
    
end

end