function [ y ] = rot_matrix_2_eulers( R, units )
%ROT_MATRIX_2_EULERS convert a rotation matrix into euler angles
%   Euler angles are ZYX gama_x, beta_y, alpha_z angles
%   Units: 'rad' (radians) or 'deg' (degrees)
%   Default units are radians. 


if nargin<2 
   units = 'rad';
else
   units = 'deg';
end


alpha_z=atan2(R(2,1),R(1,1));
beta_y=atan2(-R(3,1),abs(sqrt(R(3,2)^2+R(3,3)^2)));
gamma_x=atan2(R(3,2),R(3,3));

if beta_y>pi/2 || beta_y<-pi/2
    alpha_z = atan2(-R(2,1), -R(1,1));
    beta_y=atan2(-R(3,1),-abs(sqrt(R(3,2)^2+R(3,3)^2)));
    gamma_x = atan2(-R(3,2), -R(3,3));
end

y=[gamma_x, beta_y, alpha_z];

if units =='deg'
  y=[gamma_x, beta_y, alpha_z] * 180/pi;  
end

