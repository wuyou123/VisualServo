% Copyright (C) 
% Author asantamaria (asantamaria@iri.upc.edu)
% All rights reserved.
% 
% This file is part of VisualServo Matlab library
% VisualServo library is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>
%
%   __________________________________________________________________
%
%   [vel,V_modified] = kcontrol(lambda,J,s,s_x,k,plot_uibvs_lyapunov)
% 
%   This function computes the 6DoF camera velocities from current and
%   desired image features. Currently it is a proportional controller.
%   plane.
%    
%   Inputs:
%       - lambda:   Proportional control gain.
%       - J:        Image Jacobian.
%       - s:        Current image features.
%       - s_x:      Desired image features.
%       - k:        Current time step.
%       - plot_uibvs_lyapunov:  Enable plot of Lyapunov stability proof.
% 
%   Outputs:
%       - vel:          6DoF camera velocities.
%       - V_modified:   Modified velocity matrix.
%       - Lyap:         Lyapunov proof data.
% 
%   Copyright asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [vel,V_modified,Lyap] = kcontrol(lambda,J,s,s_x,k)

% Jacobian pseudo-inverse
J_pseudo=pinv(J);
% Proportional control
Vc(:,:,k)=-lambda*J_pseudo*(s-s_x);

% Lyapunov plot 
Lyap(1,1) = norm((s-s_x))^2/2; % Candidate function
Lyap(1,2) = -lambda*(s-s_x)'*J*J_pseudo*(s-s_x); % Derivative

vel=Vc(:,:,k);
% Lineal Velocity
v_linear(1:3,1)=Vc(1:3,1,k);
% Angular Velocity
v_angular(1:3,1)=Vc(4:6,1,k);
a=v_angular;
% Skew-symmetric matrix 
skew_v_angular= [0 -a(3) a(2) ; a(3) 0 -a(1); -a(2) a(1) 0];

% Using modified velocity matrix 
% X_dot=v_angular*X+v_linear
V_modified=[skew_v_angular, v_linear;0 0 0 0];
