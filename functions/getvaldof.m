% Copyright (C) 
% Author asantamaria (asantamaria@iri.upc.edu)
% All rights reserved.
% 
% This file is part of VisualServo Matlab library
% VisualServo library is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>
%
%   __________________________________________________________________
%
%   [x,y,z,yaw,pitch,roll] = getvaldof(T0c_str,t)
% 
%   This function returns the 6DoFs corresponding to an homogenous 
%   transform (4x4) during a period (trajectory error) 
%    
%   Inputs:
%       - T0c_str:	Trajectory (4x4 homogenous transforms)
%       - t:        Time sequence 
%   
%   Outputs:
%       - x:        X.
%       - y:        Y.
%       - z:        Z.
%       - yaw:      Yaw.
%       - pitch:    Pitch.
%       - roll:     Roll.
% 
%   Copyright asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [x,y,z,roll,pitch,yaw] = getvaldof(T0c_str,t)

if nargin==1
   t=0; 
end

for k=1:length(t)
    
x(k)=T0c_str(1,4,k);
y(k)=T0c_str(2,4,k);
z(k)=T0c_str(3,4,k);
R(:,:,k)=T0c_str(1:3,1:3,k);
vec=rot_matrix_2_eulers(R(:,:,k));
roll(k)=vec(1);
pitch(k)=vec(2);
yaw(k)=vec(3);

if nargout == 1
   x = [x;y;z;roll;pitch;yaw]; 
end

end