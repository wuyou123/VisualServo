% Copyright (C) 
% Author asantamaria (asantamaria@iri.upc.edu)
% All rights reserved.
% 
% This file is part of VisualServo Matlab library
% VisualServo library is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>
%
%   __________________________________________________________________
%
%   [cj_0,alphas] = base_rtw(pn,T0o,n)
% 
%   This function computes a basis (baricentric coord.) of the 3Dfeatures 
%   points (pn). This points are retrieved in cj_0 (control points). 
%   It also outputs the alphas. Refer to the following paper 
%   for more details on these outputs:
% 
%   A. Santamaria-Navarro and J. Andrade-Cetto, 
%   Uncalibrated image-based visual servoing, 
%   IEEE International Conference on Robotics and Automation,
%   2013, Karlsruhe, pp. 5227-5232.
%    
%   Inputs:
%       - pn:       3D feature points
%   
%   Outputs:
%       - cj_0:     Control points 
%       - alphas:   Alphas (aij's)
% 
%   Copyright asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [cj_0,alphas] = base_rtw(pn)

disp('Computing base points');

% alphas
cj_0(:,1)=mean((pn(1:3,:)),2);
q=pn(1:3,:)-repmat(cj_0(:,1),1,size(pn,2));
[~,S,V]=svd(q*q');
cj_0(:,2)=cj_0(:,1)+V(:,1);
cj_0(:,3)=cj_0(:,1)+V(:,2);

% In the non planar case
if(rank(S)>2)
disp('  \_NON Planar case');       
cj_0(:,4)=cj_0(:,1)+V(:,3);
% aij's associated
alphas(2:4,:) = inv([cj_0(:,2)-cj_0(:,1) cj_0(:,3)-cj_0(:,1) cj_0(:,4)-cj_0(:,1)])*q;
alphas(1,:)=ones(1,size(pn,2))-sum(alphas(2:4,:)); 

else
disp('> Planar case');   
% aij's associated
alphas(2:3,:) = pinv([cj_0(:,2)-cj_0(:,1) cj_0(:,3)-cj_0(:,1)])*q;
alphas(1,:)=ones(1,size(pn,2))-sum(alphas(2:3,:));
end

