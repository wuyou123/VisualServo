% Copyright (C) 
% Author asantamaria (asantamaria@iri.upc.edu)
% All rights reserved.
% 
% This file is part of VisualServo Matlab library
% VisualServo library is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>
%
%   __________________________________________________________________
%
%   [ex,ey,ez,eyaw,epitch,eroll] = geterror(t,Tgt,T)
% 
%   This function computes the pose error between two homogenous 
%   transforms (4x4) during a period (trajectory error) 
%    
%   Inputs:
%       - t:    Time sequence 
%       - Tgt:  Ground truth (4x4 homogenous transforms)
%       - T:    Trajectory (4x4 homogenous transforms)
%   
%   Outputs:
%       - pose: Pose error vector (ex,ey,ez,eroll,epitch,eyaw).
% 
%   Copyright asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [epose] = geterror(t,Tgt,T)

% Ground truth
[x_gt,y_gt,z_gt,yaw_gt,pitch_gt,roll_gt]=getvaldof(Tgt,1);

% Data
[x,y,z,yaw,pitch,roll]=getvaldof(T,t);

% Error
ex = x-x_gt;
ey = y-y_gt;
ez = z-z_gt;
eyaw = yaw-yaw_gt;
epitch = pitch-pitch_gt;
eroll = roll-roll_gt;

epose = [ex;ey;ez;eroll;epitch;eyaw];

end