function [uv_x] = getprojection(T,cj_0,u0,v0,f_real,noise)

% Base coordinates r.t Desired frame 
cP_x = T*[cj_0;ones(1,length(cj_0))];  
    
% Camera projection of desired features only to plot
[uv_x]=imageprojection(f_real,f_real,u0,v0,cP_x);
uv_x=uv_x+noise*randn(2,length(uv_x));
    
end