% Copyright (C) 
% Author asantamaria (asantamaria@iri.upc.edu)
% All rights reserved.
% 
% This file is part of VisualServo Matlab library
% VisualServo library is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>
%
%   __________________________________________________________________
%
%   [s] = featuresvector(cP)
% 
%   This function returns a vector with 2D features from 3D feature points.
%    
%   Inputs:
%       - cP:   3D features vector.
%   
%   Outputs:
%       - s:    2D features vector.
% 
%   Copyright asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [s] = featuresvector(cP)
  
% Features vector
xxs=(cP(1,:)./cP(3,:));
yys=(cP(2,:)./cP(3,:));
len=length(xxs);
xys=cat(1,xxs,yys);
s=reshape(xys,2*len,1); 