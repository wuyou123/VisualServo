% Copyright (C) 
% Author asantamaria (asantamaria@iri.upc.edu)
% All rights reserved.
% 
% This file is part of VisualServo Matlab library
% VisualServo library is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>
%
%   __________________________________________________________________
%
%   [uv] = imageprojection(fu,fv,u0,v0,cP)
% 
%   This function computes the projection of a 3D point onto the image
%   plane.
%    
%   Inputs:
%       - fu:   Horizontal focal length.
%       - fv:   Vertical focal length.
%       - u0:   Image center (horizontal coordinate).
%       - v0:   Image center (vertical coordinate).    
%       - cP:   3D point. 
%   
%   Outputs:
%       - uv:   2D image point.   
% 
%   Copyright asantamaria@iri.upc.edu.
%   __________________________________________________________________

function [uv] = imageprojection(fu,fv,u0,v0,cP)

% image projection
u=(cP(1,:)./cP(3,:))*fu+u0;
v=(cP(2,:)./cP(3,:))*fv+v0;
uv=cat(1,u,v);

end