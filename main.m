% Copyright (C) 
% Author asantamaria (asantamaria@iri.upc.edu)
% All rights reserved.
% 
% This file is part of VisualServo Matlab library
% VisualServo library is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% at your option) any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>
%
%   __________________________________________________________________
%
%   Visual Servoing
% 
%   Visual servoing library to drive a holonomic camera (6DoF).
%
%   Currently implemented visual servo (VS) schemes: 
%       - Classical image-based VS (IBVS)
%       - Uncalibrated image-based VS (UIBVS) 
%       - Classical position-based VS (PBVS) 
%
%   IMPORTANT NOTE: This library is not optimized, please use the C++
%   implementation for scientific comparison purposes. 
% 
%   If you use this code, please cite:
%
%   A. Santamaria-Navarro, P. Grosch, V. Lippiello, 
%   J. Sol? and J. Andrade-Cetto. "Uncalibrated Visual Servo for 
%   Unmanned Aerial Manipulation", 2017, IEEE/ASME Transactions 
%   on Mechatronics, vol. 22, no. 4, pp. 1610-1621.
%
%   A. Santamaria-Navarro and J. Andrade-Cetto, 
%   "Uncalibrated image-based visual servoing", 
%   IEEE International Conference on Robotics and Automation,
%   2013, Karlsruhe, pp. 5227-5232.
%
%   V. Lippiello, J. Cacace, A. Santamaria-Navarro, J. Andrade-Cetto, 
%   M.A. Trujillo, Y. Rodriguez Esteves and A. Viguria, 
%   "Hybrid Visual Servoing with Hierarchical Task Composition 
%   for Aerial Manipulation", IEEE Robotics and Automation Letters, 
%   2016, vol. 1, no. 1, pp. 259-266.
% 
%   Copyright asantamaria@iri.upc.edu.
%   __________________________________________________________________

% clean ws
clc
clear
close all

% Add paths
addpath data;
addpath(genpath('functions'));

% Plot options
plot_opts.live_plot = false;        % All plots simultaneously live (warn: heavy comp.)
plot_opts.imgplane_2Dtraj = true;   % Feature trajectories in image plane 
plot_opts.feat_error = false;       % Feature errors (e = [u* v*]^T - [u v]^T])
plot_opts.campose_error = false;    % Camera pose error (6DoF)
plot_opts.cam_vel = false;          % Camera velocity (6DoF)
plot_opts.cam_3Dtraj = false;       % Camera trajectory (3D)
plot_opts.uibvs_lyapunov = false;   % Lyapunov stability proof (UIBVS)

% Algorithms parameters
[Tc_des,T0c_0,T0o,lambda,Ts,t,diTj,pn,u0,v0,f_real,f_ibvs,f_uibvs,f_pbvs,hres,vres,noise] = ini_params();

% Get a basis (baricentric coordinates of 3D feature points
[cj_0,alphas] = base_rtw(pn);

%% IBVS: Classical image-based visual servo (calibrated) __________________
[Vc_ibvs,T0c_ibvs,uv_ibvs] = ibvs(T0o,Tc_des,T0c_0,cj_0,f_ibvs,f_real,u0,v0,lambda,t,Ts,diTj,noise);

%% UIBVS: Uncalibrated image-based visual servo ___________________________
[Vc_uibvs,T0c_uibvs,uv_uibvs,f_uibvs,Lyap] = uibvs(T0o,Tc_des,T0c_0,f_uibvs,f_real,u0,v0,lambda,t,Ts,diTj,cj_0,pn,alphas,noise);

%% PBVS: Classical position-based visual servo (calibrated) _______________
[Vc_pbvs,T0c_pbvs,uv_pbvs] = pbvs(T0o,Tc_des,T0c_0,cj_0,f_pbvs,f_real,u0,v0,lambda,t,Ts,diTj,noise,pn);


%% Plots __________________________________________________________________

disp('Plotting');

% Get desired feature coordinates
uv_ini = getprojection(inv(T0c_0)*T0o,cj_0,u0,v0,f_real,noise);
uv_des = getprojection(inv(Tc_des)*T0o,cj_0,u0,v0,f_real,noise);
    
% Camera update
c_ibvs = camera(f_ibvs,f_ibvs,u0,v0,hres,vres);  
c_uibvs = camera(f_uibvs,f_uibvs,u0,v0,hres,vres);
c_pbvs = camera(f_pbvs,f_pbvs,u0,v0,hres,vres);

% Pose error to plot
[ibvs_epose] = geterror(t,Tc_des,T0c_ibvs);
[uibvs_epose] = geterror(t,Tc_des,T0c_uibvs);
[pbvs_epose] = geterror(t,Tc_des,T0c_pbvs);

% Live plots
if plot_opts.live_plot
    plot_live(t,uv_des,uv_ibvs,uv_uibvs,uv_pbvs,Vc_ibvs,Vc_uibvs,Vc_pbvs,...
             c_ibvs,T0c_ibvs,pn,cj_0,c_uibvs,T0c_uibvs,c_pbvs,T0c_pbvs,...
             ibvs_epose,uibvs_epose,pbvs_epose,plot_opts);  
else
   
% Feature trajectories in image plane
if plot_opts.imgplane_2Dtraj
    plot_pointstraj(2,uv_ini,uv_des,uv_ibvs,uv_uibvs,uv_pbvs,'Image plane trajectories');
end

% Feature errors (e = [u* v*]^T - [u v]^T])
if plot_opts.feat_error   
    plot_featerror(3,t,uv_ibvs,uv_des,'IBVS: Features error');
    plot_featerror(4,t,uv_uibvs,uv_des,'UIBVS: Features error');
    plot_featerror(5,t,uv_pbvs,uv_des,'PBVS: Features error');
end

% Camera pose error (6DoF)
if plot_opts.campose_error
    plot_campose(6,t,ibvs_epose,'IBVS: Cam position and orientation (m;rad)');
    plot_campose(7,t,uibvs_epose,'UIBVS: Cam position and orientation (m;rad)');
    plot_campose(8,t,pbvs_epose,'PBVS: Cam position and orientation (m;rad)');
end

% Camera velocity (6DoF)
if plot_opts.cam_vel
    plot_velocity(9,t,Vc_ibvs,'IBVS: Velocity plots (m/s;rad/s)');
    plot_velocity(10,t,Vc_uibvs,'UIBVS: Velocity plots (m/s;rad/s)');
    plot_velocity(11,t,Vc_pbvs,'PBVS: Velocity plots (m/s;rad/s)');
end

% Camera trajectory (3D)
if plot_opts.cam_3Dtraj
    plot_camtraj(12,c_ibvs,T0c_ibvs,pn,cj_0,'IBVS: Camera trajectory');
    plot_camtraj(13,c_uibvs,T0c_uibvs,pn,cj_0,'UIBVS: Camera trajectory');
    plot_camtraj(14,c_pbvs,T0c_pbvs,pn,cj_0,'PBVS: Camera trajectory');
end

% Lyapunov stability proof (UIBVS)
if plot_opts.uibvs_lyapunov
    plot_lyapunov(15,t,Lyap,'UIBVS: Lyapunov proof');  
end

%     % Distribute figures
%     distFig('Only',[3,4,5,6,7,8]);
    
end





